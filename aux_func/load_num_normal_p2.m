function [normal] = load_num_normal_p2(surf_name,n,Nodes,Elements_plot,grad_fd)
% 
% Loading or precomputing (and projecting) P2 mesh for KLL algorithm.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if exist(['../surfs/',surf_name,'_normal_p2_',num2str(n),'.txt'], 'file')==2
        normal=load(['../surfs/',surf_name,'_normal_p2_',num2str(n),'.txt']);
    else
        if ~ischar(grad_fd)
            pre_normal = grad_fd(Nodes);
            normal = pre_normal ./ sqrt(sum(pre_normal.^2, 2));
            save(['../surfs/',surf_name,'_normal_p2_',num2str(n),'.txt'],'normal', '-ASCII');
        else
            normal = compute_normal(Nodes,Elements_plot);
            save(['../surfs/',surf_name,'_normal_p2_',num2str(n),'.txt'],'normal', '-ASCII');
        end
end
end

function normal_vec = compute_normal(Nodes,Elements_plot)

normal_vec = zeros(length(Nodes),3);

E = Elements_plot;
E(:,4) = 0;

list_of_Elements = ( 1 : length(E) ).';

normals_on_elements = zeros(length(E),3);

for ind = 1 : length(Nodes)
    % determine patch around the node
    patch_ind_at_node = ((E(:,1)-ind).^2 < 0.5 ) | ((E(:,2)-ind).^2 < 0.5 ) | ((E(:,3)-ind).^2 < 0.5 );
    patch_list_at_node = list_of_Elements(patch_ind_at_node);
    
    % compute the normals along the patch, and average it
    normal_at_node = [0 0 0];
    for pind = patch_list_at_node.'
%         hold on;
        if E(pind,4) == 0
            % compute the normal (positively oriented elements)
            normal_pre_norm = cross(Nodes(E(pind,2),:)-Nodes(E(pind,1),:),Nodes(E(pind,3),:)-Nodes(E(pind,1),:));
            normal_on_element = normal_pre_norm/norm(normal_pre_norm);
            
%             trisurf(E(pind,1:3),Nodes(:,1),Nodes(:,2),Nodes(:,3), 0 );
%             C_E_pind = sum( Nodes(E(pind,1:3),:) , 1 ) / 3;
%             quiver3(C_E_pind(:,1),C_E_pind(:,2),C_E_pind(:,3),normal_on_element(:,1),normal_on_element(:,2),normal_on_element(:,3))
                        
            % add it to normal
            normal_at_node = normal_at_node + normal_on_element;
            % save and mark as computed
            normals_on_elements(pind,:) = normal_on_element;
            E(pind,4) = 1;
        elseif E(pind,4) == 1
            % the normal is already computed, add it to average
            normal_at_node = normal_at_node + normals_on_elements(pind,:);
        end
    end
%     hold off;
%     drawnow
    normal_vec(ind,:) = normal_at_node / norm(normal_at_node);
%     disp('')
end


end
