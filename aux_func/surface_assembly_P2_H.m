function [M,H] = surface_assembly_P2_H(Nodes,Elements, Normal)
% Surface assembly of mass matrix (M) and discrete mean curvature.
% 
% - two dimensional surfaces in three dimensions
% - quadratic isoparametric finite elements
%
% Written in P2Q2Iso2D approach.
% Using cell structure conversion for sparse matrices;
% see: https://de.mathworks.com/matlabcentral/answers/203734-most-efficient-way-to-add-multiple-sparse-matrices-in-a-loop-in-matlab
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

format long;

% the quadrature nodes and the number of quadrature nodes
W=[0.112500000000000   0.066197076394253   0.066197076394253   0.066197076394253   0.062969590272414   0.062969590272414   0.062969590272414];
length_W = length(W);

%% load or compute and save the (precomputed) product tensors
if exist(['assembly_precomp_P2/precomp_values_P2_',num2str(length_W),'.mat'], 'file')==2
    precomp_values=load(['assembly_precomp_P2/precomp_values_P2_',num2str(length_W),'.mat']);
    F_eval = precomp_values.F_eval;
    M_eval = precomp_values.M_eval;
    grad_F_eval = precomp_values.grad_F_eval;
else
    disp(['Computing and storing precomputeable values (M=',num2str(length_W),').'])
    % Dunavant quadrature of order 5 nodes and weights
    xieta = [0.333333333333333   0.059715871789770   0.470142064105115   0.470142064105115   0.797426985353087   0.101286507323456 0.101286507323456;
             0.333333333333333   0.470142064105115   0.470142064105115   0.059715871789770   0.101286507323456   0.101286507323456 0.797426985353087;
                             0                   0                   0                   0                   0                   0                 0];
    % precomputing data                     
    [F_eval,M_eval, grad_F_eval] = surface_assembly_precompute_for_P2(xieta,W);
    % saving data in the right directory, and creating directory if it not exists
    if exist('assembly_precomp_P2', 'dir') ~= 7
        mkdir('assembly_precomp_P2');
    end
    save(['assembly_precomp_P2/precomp_values_P2_',num2str(length_W),'.mat'],'F_eval','M_eval','grad_F_eval');
end

%% inicialization
% local gegrees of freedom, and full one vector
loc_dof = 6; % due to quadratic elements
e_loc_dof = ones(1,loc_dof);
% degree of freedom
dof = length(Nodes);


%% modifications of the loaded data
% !!! should be rewritten to best fit the code --> 
pa_xi_F_eval(1:6,1:length_W)=grad_F_eval(1,:,:);
pa_eta_F_eval(1:6,1:length_W)=grad_F_eval(2,:,:);
% <-- should be rewritten to best fit the code !!!

%% assembly

M_Cell = cell(size(Elements,1),1);
H_Cell = cell(size(Elements,1),1);

for i=1:size(Elements,1) % begin loop over elements
    %% element transofrmation and evaluations (combining the above two ommented blocks)
    % element array rows
    Element_array_rows = reshape(e_loc_dof .* Elements(i,:).',[36 1]);
    Element_array_columns = reshape(Elements(i,:) .* e_loc_dof.',[36 1]);
    Element_array_rows_1 = Elements(i,:).';
    % Note: for symmetric matrices the do not matter!
    
    % vertices of current element (as a 6 x 3 matrix)
    nodes_of_element = Nodes(Elements(i,:),:);
    
    % the partial derivatives of the element transofrmation map
    pa_xi_Phi = nodes_of_element.' * pa_xi_F_eval;
    pa_eta_Phi = nodes_of_element.' * pa_eta_F_eval;
    % the third column of the Jacobian of the element transformation map
    % (the cross product of the two partial derivatives)
    D_3 = cross(pa_xi_Phi,pa_eta_Phi);
    % length (Euclidean norm) of the cross product
    nrm_cross = vecnorm(D_3,2,1);
    
    % the Jacobian of the element transformation map
    D(1:3,1,:) = pa_xi_Phi;
    D(1:3,2,:) = pa_eta_Phi;
    D(1:3,3,:) = D_3;
    
    % the pointwise product of the quadrature weights and the norm of the cross product
    prod_W_nrm = (W .* nrm_cross).';
    
    % local nodal values of the input vector u
    Normal_loc = Normal(Elements(i,:),:);
    
    %% mass matrix assembly
    % M|_{kj} = \int_{\Ga_h} \phi_j \phi_k
   
    % the vectorized local matrix (the sum for the quadrature)
    MLoc_vec = reshape(M_eval,[36 length_W]) * prod_W_nrm;
    
    M_Cell{i} = [Element_array_rows Element_array_columns MLoc_vec];
    
    %% H vector assembly
    % H|_{k} = \int_{\Ga_h} trace( \nb_{\Ga_h} \nu_h )  \phi_k
    % inverting the Jacobians, computing gradients of basis functions, and performing further calculations
    for ell = 1:length_W
%         C(:,:,ell) = inv(D(:,:,ell)); % alternative: D(:,:,ell)\eye(3);
%         B(:,:,ell) = C(:,:,ell).' * grad_F_eval(:,:,ell);
%         BtB(:,:,ell) = B(:,:,ell).' * B(:,:,ell);
        C_ell = inv(D(:,:,ell)); 
        B_ell = C_ell.' * grad_F_eval(:,:,ell);
        
        % computing discrete mean curvature
        A = C_ell.' * (grad_F_eval(:,:,ell) * Normal_loc);
        H_locvec(ell,1) = trace( C_ell.' * (grad_F_eval(:,:,ell) * Normal_loc) );
    end
    
    % the vectorized local matrix (the sum for the quadrature)
    HLoc_vec = (prod_W_nrm .* H_locvec).' *  F_eval;
    
    H_Cell{i} = [Element_array_rows_1 e_loc_dof.' HLoc_vec.'];
    

end % end loop over elements

M_Cell_to_sparse = cell2mat( M_Cell );
M = sparse(M_Cell_to_sparse(:,1),M_Cell_to_sparse(:,2),M_Cell_to_sparse(:,3),dof,dof);

H_Cell_to_sparse = cell2mat( H_Cell );
H_vec = sparse(H_Cell_to_sparse(:,1),H_Cell_to_sparse(:,2),H_Cell_to_sparse(:,3),dof,1);

H = full(M \ H_vec);

end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [F_eval,M_eval, grad_F_eval]=surface_assembly_precompute_for_P2(xieta,W)
% This code precomputes and saves various evaluations of reference basis functions,
% and products of such evaluations in the quadrature nodes.
% 
% - standard reference element (with nodes (0,0), (1,0), (0,1), (0.5,0), (0.5,0.5), (0,0.5))
% - standard quadratic finite elements
% 
% Written for P2Q2 approach.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% basis functions (or form functions) on the reference element
% Anonymous functions are used, for easy modification for other element types.
F=@(xi,eta)[(2*xi+2*eta-1).*(xi+eta-1) xi.*(2*xi-1) eta.*(2*eta-1)  -4*xi.*(xi+eta-1) 4*xi.*eta -4*eta.*(xi+eta-1)];

% evaluating the basis functions on the reference element
F_eval=[];
for ind=1:length(W)
    F_eval=[F_eval ; F(xieta(1,ind),xieta(2,ind))];
end

% Mass matrix assembly: This code precomputes and saves 
% the 6 x 6 x M tensor containing the products of the
% basis functions on the reference element:
%   M_eval|_{kjl} = \vphi_k(xi_l) \vphi_j(xi_l)
M_eval=zeros(6,6,length(W));
for ell=1:length(W) 
    F_ell=F_eval(ell,:);
    M_eval(:,:,ell)=F_ell.'*F_ell;
end

% Stiffness matrix assembly: 
% Evaluating gradients of the basis functions in the quadrature nodes.
% grad F1
grad_F1=@(xi,eta) [4*xi + 4*eta - 3;4*xi + 4*eta - 3;0];
% grad F2
grad_F2=@(xi,eta) [4*xi - 1;0;0];
% grad F3
grad_F3=@(xi,eta) [0;4*eta - 1;0];

% grad F4
grad_F4=@(xi,eta) [-4*(2*xi + eta - 1);-4*xi;0];
% grad F5
grad_F5=@(xi,eta) [4*eta;4*xi;0];
% grad F6
grad_F6=@(xi,eta) [-4*eta;-4*(xi + 2*eta - 1);0];

for ind=1:length(W)
    grad_F_eval(:,:,ind)=[grad_F1(xieta(1,ind),xieta(2,ind)) grad_F2(xieta(1,ind),xieta(2,ind)) grad_F3(xieta(1,ind),xieta(2,ind))  grad_F4(xieta(1,ind),xieta(2,ind)) grad_F5(xieta(1,ind),xieta(2,ind)) grad_F6(xieta(1,ind),xieta(2,ind))];
end
end