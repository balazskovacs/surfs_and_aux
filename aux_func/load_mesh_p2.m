function [Nodes,Elements,Elements_plot] = load_mesh_p2(surf_name,fd)
% 
% Loading or precomputing (and projecting) P2 mesh for KLL algorithm.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if exist(['../surfs/',surf_name,'_nodes_p2.txt'], 'file')==2
        Nodes=load(['../surfs/',surf_name,'_nodes_p2.txt']);
        Elements=load(['../surfs/',surf_name,'_elements_p2.txt']);
        Elements_plot=load(['../surfs/',surf_name,'_elements_plot_p2.txt']);
    else
        Nodes=load(['../surfs/',surf_name,'_nodes.txt']);
        Elements=load(['../surfs/',surf_name,'_elements.txt']);
        [~,~,Nodes,Elements,Elements_plot]=preprocess(Nodes,Elements,2);
        
        if ~ischar(fd)
            % projecting back to the surface using distance function fd
            % based on DistMesh
%             Nodes = lift(Nodes); % for Angenent torus
            d=feval(fd,Nodes);
            deps=sqrt(eps);
            dgradx=(feval(fd,[Nodes(:,1)+deps,Nodes(:,2),Nodes(:,3)])-d)/deps; % Numerical
            dgrady=(feval(fd,[Nodes(:,1),Nodes(:,2)+deps,Nodes(:,3)])-d)/deps; % gradient
            dgradz=(feval(fd,[Nodes(:,1),Nodes(:,2),Nodes(:,3)+deps])-d)/deps; %
            dgrad2=dgradx.^2+dgrady.^2+dgradz.^2;
            Nodes=Nodes-[d.*dgradx./dgrad2,d.*dgrady./dgrad2,d.*dgradz./dgrad2];  % Project back to boundary

            % saving
            save(['../surfs/',surf_name,'_nodes_p2.txt'], 'Nodes', '-ASCII');
            save(['../surfs/',surf_name,'_elements_p2.txt'], 'Elements', '-ASCII');
            save(['../surfs/',surf_name,'_elements_plot_p2.txt'], 'Elements_plot', '-ASCII');
        elseif strcmp(fd,'unknown')
            % if distance function is unknown do nothing
            
            % saving
            save(['../surfs/',surf_name,'_nodes_p2.txt'], 'Nodes', '-ASCII');
            save(['../surfs/',surf_name,'_elements_p2.txt'], 'Elements', '-ASCII');
            save(['../surfs/',surf_name,'_elements_plot_p2.txt'], 'Elements_plot', '-ASCII');
        end
end