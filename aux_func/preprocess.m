function [Nodes,Elements,Nodes_new,Elements_new,Elements_new2]=preprocess(Nodes,Elements,poly_deg) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mesh preprocessing for higher order surface FEM.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;

%% Edges
Edges(1,:)=[Elements(1,1) Elements(1,2) 1 0];
Edges(2,:)=[Elements(1,2) Elements(1,3) 1 0];
Edges(3,:)=[Elements(1,3) Elements(1,1) 1 0];

nElemek=size(Elements,1);
for i_elemek=2:nElemek
    a=Elements(i_elemek,1);
    b=Elements(i_elemek,2);
    c=Elements(i_elemek,3);
    kezelt_ab=0;
    kezelt_bc=0;
    kezelt_ca=0;
    
    % gyors�t�s [tf loc]=ismember(A, S, 'rows')
    nElek=size(Edges,1);
    for i_elek=1:nElek
        % [a b] �l
        if isequal([a b] , Edges(i_elek,1:2))
            Edges(i_elek,4)=i_elemek;
            kezelt_ab=1;
        elseif isequal([b a] , Edges(i_elek,1:2))
            Edges(i_elek,4)=i_elemek;
            kezelt_ab=1;
        end
        
        % [b c] �l
        if isequal([b c] , Edges(i_elek,1:2))
            Edges(i_elek,4)=i_elemek;
            kezelt_bc=1;
        elseif isequal([c b] , Edges(i_elek,1:2))
            Edges(i_elek,4)=i_elemek;
            kezelt_bc=1;
        end
        
        % [c a] �l
        if isequal([c a] , Edges(i_elek,1:2))
            Edges(i_elek,4)=i_elemek;
            kezelt_ca=1;
        elseif isequal([a c] , Edges(i_elek,1:2))
            Edges(i_elek,4)=i_elemek;
            kezelt_ca=1;
        end
    end

    if kezelt_ab == 0
        Edges=[Edges;[a b i_elemek 0]];
    end
    
    if kezelt_bc == 0
        Edges=[Edges;[b c i_elemek 0]];
    end
    
    if kezelt_ca == 0
        Edges=[Edges;[c a i_elemek 0]];
    end
end

%% new elements for hiher order surface FEM (( pol deg <= 4 ))
switch poly_deg
    case 1
        % % pol_fok=2
        Nodes_new=Nodes;
        Elements_new=Elements;
        Elements_new2=Elements;
    case 2
        % % pol_fok=2
        csucsok_szama=size(Nodes,1);
        elek_szama=size(Edges,1);
        elemek_szama=size(Elements,1);

        Nodes_new=zeros(csucsok_szama + 1*elek_szama,3);
        Nodes_new(1:csucsok_szama,:)=Nodes;
        
        Elements_new=zeros(elemek_szama,2*3);
        Elements_new(:,1:3)=Elements;
        uj_csucs=csucsok_szama+1;
        
        for i_elek=1:elek_szama
            el=Edges(i_elek,1:2);

            for i_elemek=1:nElemek
                haromszog=Elements(i_elemek,:);
                a=haromszog(1);
                b=haromszog(2);
                c=haromszog(3);
                
                % sima
                if isequal([a b] , el)
                    Elements_new(i_elemek,4)=uj_csucs;
                    uj_csucs_koord=(Nodes(a,:)+Nodes(b,:))/2;
                elseif isequal([b c] , el)
                    Elements_new(i_elemek,5)=uj_csucs;
                    uj_csucs_koord=(Nodes(c,:)+Nodes(b,:))/2;
                elseif isequal([c a] , el)
                    Elements_new(i_elemek,6)=uj_csucs;
                    uj_csucs_koord=(Nodes(a,:)+Nodes(c,:))/2;
                end
                
                % forditva
                if isequal([b a] , el)
                    Elements_new(i_elemek,4)=uj_csucs;
                    uj_csucs_koord=(Nodes(a,:)+Nodes(b,:))/2;
                elseif isequal([c b] , el)
                    Elements_new(i_elemek,5)=uj_csucs;
                    uj_csucs_koord=(Nodes(c,:)+Nodes(b,:))/2;
                elseif isequal([a c] , el)
                    Elements_new(i_elemek,6)=uj_csucs;
                    uj_csucs_koord=(Nodes(a,:)+Nodes(c,:))/2;
                end
            end
            Nodes_new(uj_csucs,:)=uj_csucs_koord;
            uj_csucs=uj_csucs+1;
        end     
        
        % s�r� elemek
        Elements_new2=zeros(elemek_szama*poly_deg^2,3);
        for i_elemek=1:elemek_szama
            % elem=Elemek_bovitett(i_elemek,:);
            
            % positive orientation (clockwise)
            uj_elemek(1,:)=Elements_new(i_elemek,[1 4 6]);
            uj_elemek(2,:)=Elements_new(i_elemek,[4 5 6]);
            uj_elemek(3,:)=Elements_new(i_elemek,[4 2 5]);
            uj_elemek(4,:)=Elements_new(i_elemek,[5 3 6]);

            Elements_new2(1+(i_elemek-1)*4:4+(i_elemek-1)*4,:)=uj_elemek;
        end
%     case 3
%         % % pol_fok=3
%         csucsok_szama=size(Nodes,1);
%         elek_szama=size(Elek,1);
%         elemek_szama=size(Elements,1);
% 
%         Nodes_new=zeros(csucsok_szama + 2*elek_szama + elemek_szama,3);
%         Nodes_new(1:csucsok_szama,:)=Nodes;
%         
%         Elements_new=zeros(elemek_szama,3+3*2+1);
%         Elements_new(:,1:3)=Elements;
%         uj_csucs=csucsok_szama+1;
%         uj_neumann=1;
%         
%         for i_elek=1:elek_szama
%             el=Elek(i_elek,1:2);
%             for i_elemek=1:nElemek
%                 haromszog=Elements(i_elemek,:);
%                 a=haromszog(1);
%                 b=haromszog(2);
%                 c=haromszog(3);
%                 
%                 % sima
%                 if isequal([a b] , el)
%                     Elements_new(i_elemek,4)=uj_csucs;
%                     Elements_new(i_elemek,5)=uj_csucs+1;
%                     uj_csucs_koord0=(2*Nodes(a,:)+Nodes(b,:))/3;
%                     uj_csucs_koord1=(Nodes(a,:)+2*Nodes(b,:))/3;
%                 elseif isequal([b c] , el)
%                     Elements_new(i_elemek,6)=uj_csucs;
%                     Elements_new(i_elemek,7)=uj_csucs+1;
%                     uj_csucs_koord0=(2*Nodes(b,:)+Nodes(c,:))/3;
%                     uj_csucs_koord1=(Nodes(b,:)+2*Nodes(c,:))/3;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([b c], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[b uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs+1 c];
%                         uj_neumann=uj_neumann+3;
%                     end
%                 elseif isequal([c a] , el)
%                     Elements_new(i_elemek,8)=uj_csucs;
%                     Elements_new(i_elemek,9)=uj_csucs+1;
%                     uj_csucs_koord0=(2*Nodes(c,:)+Nodes(a,:))/3;
%                     uj_csucs_koord1=(Nodes(c,:)+2*Nodes(a,:))/3;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([c a], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[c uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs+1 a];
%                         uj_neumann=uj_neumann+3;
%                     end
%                 end
%                 
%                 % forditva
%                 if isequal([b a] , el)
%                     Elements_new(i_elemek,5)=uj_csucs;
%                     Elements_new(i_elemek,4)=uj_csucs+1;
%                     uj_csucs_koord0=(2*Nodes(b,:)+Nodes(a,:))/3;
%                     uj_csucs_koord1=(Nodes(b,:)+2*Nodes(a,:))/3;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([b a], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[b uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs+1 uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs a];
%                         uj_neumann=uj_neumann+3;
%                     end
%                 elseif isequal([c b] , el)
%                     Elements_new(i_elemek,7)=uj_csucs;
%                     Elements_new(i_elemek,6)=uj_csucs+1;
%                     uj_csucs_koord0=(2*Nodes(c,:)+Nodes(b,:))/3;
%                     uj_csucs_koord1=(Nodes(c,:)+2*Nodes(b,:))/3;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([c b], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[c uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs+1 uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs b];
%                         uj_neumann=uj_neumann+3;
%                     end
%                 elseif isequal([a c] , el)
%                     Elements_new(i_elemek,9)=uj_csucs;
%                     Elements_new(i_elemek,8)=uj_csucs+1;
%                     uj_csucs_koord0=(2*Nodes(a,:)+Nodes(c,:))/3;
%                     uj_csucs_koord1=(Nodes(a,:)+2*Nodes(c,:))/3;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([a c], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[a uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs+1 uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs c];
%                         uj_neumann=uj_neumann+3;
%                     end
%                 end
%             end
%             Nodes_new(uj_csucs,:)=uj_csucs_koord0;
%             Nodes_new(uj_csucs+1,:)=uj_csucs_koord1;
%             uj_csucs=uj_csucs+2;
%         end
%         
%         % bubble points
%         for i_elemek=1:nElemek
%             haromszog=Elements(i_elemek,:);
%             a=haromszog(1);
%             b=haromszog(2);
%             c=haromszog(3);
%             
%             Elements_new(i_elemek,10)=uj_csucs;
%             Nodes_new(uj_csucs,:)=(Nodes(a,:)+Nodes(b,:)+Nodes(c,:))/3;
%             uj_csucs=uj_csucs+1;
%         end
%         
%         % "s�r�" elemek
%         Elements_new2=zeros(elemek_szama*poly_deg^2,3);
%         for i_elemek=1:elemek_szama
%             elem=Elements_new(i_elemek,:);
% 
%             uj_elemek(1,:)=Elements_new(i_elemek,[1 4 9]);
%             uj_elemek(2,:)=Elements_new(i_elemek,[4 5 10]);
%             uj_elemek(3,:)=Elements_new(i_elemek,[4 10 9]);
%             uj_elemek(4,:)=Elements_new(i_elemek,[8 9 10]);
%             uj_elemek(5,:)=Elements_new(i_elemek,[7 8 10]);
%             uj_elemek(6,:)=Elements_new(i_elemek,[3 8 7]);
%             uj_elemek(7,:)=Elements_new(i_elemek,[6 7 10]);
%             uj_elemek(8,:)=Elements_new(i_elemek,[5 6 10]);
%             uj_elemek(9,:)=Elements_new(i_elemek,[2 6 5]);
% 
%             Elements_new2(1+(i_elemek-1)*9:9+(i_elemek-1)*9,:)=uj_elemek;
%         end
%     case 4
%         % % pol_fok=4
%         csucsok_szama=size(Nodes,1);
%         elek_szama=size(Elek,1);
%         elemek_szama=size(Elements,1);
% 
%         Nodes_new=zeros(csucsok_szama + 3*elek_szama + 3*elemek_szama,3);
%         Nodes_new(1:csucsok_szama,:)=Nodes;
%         
%         Elements_new=zeros(elemek_szama,3+3*3+3);
%         Elements_new(:,1:3)=Elements;
%         uj_csucs=csucsok_szama+1;
%         uj_neumann=1;
%         
%         for i_elek=1:elek_szama
%             el=Elek(i_elek,1:2);
%             for i_elemek=1:nElemek
%                 haromszog=Elements(i_elemek,:);
%                 a=haromszog(1);
%                 b=haromszog(2);
%                 c=haromszog(3);
%                 
%                 % sima
%                 if isequal([a b] , el)
%                     Elements_new(i_elemek,4)=uj_csucs;
%                     Elements_new(i_elemek,5)=uj_csucs+1;
%                     Elements_new(i_elemek,6)=uj_csucs+2;
%                     uj_csucs_koord0=(3*Nodes(a,:)+Nodes(b,:))/4;
%                     uj_csucs_koord1=(2*Nodes(a,:)+2*Nodes(b,:))/4;
%                     uj_csucs_koord2=(Nodes(a,:)+3*Nodes(b,:))/4;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([a b], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[a uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs+1 uj_csucs+2];
%                         Neumann_elek_bovitett(uj_neumann+3,:)=[uj_csucs+2 b];
%                         uj_neumann=uj_neumann+4;
%                     end
%                 elseif isequal([b c] , el)
%                     Elements_new(i_elemek,7)=uj_csucs;
%                     Elements_new(i_elemek,8)=uj_csucs+1;
%                     Elements_new(i_elemek,9)=uj_csucs+2;
%                     uj_csucs_koord0=(3*Nodes(b,:)+Nodes(c,:))/4;
%                     uj_csucs_koord1=(2*Nodes(b,:)+2*Nodes(c,:))/4;
%                     uj_csucs_koord2=(Nodes(b,:)+3*Nodes(c,:))/4;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([b c], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[b uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs+1 uj_csucs+2];
%                         Neumann_elek_bovitett(uj_neumann+3,:)=[uj_csucs+2 c];
%                         uj_neumann=uj_neumann+4;
%                     end
%                 elseif isequal([c a] , el)
%                     Elements_new(i_elemek,10)=uj_csucs;
%                     Elements_new(i_elemek,11)=uj_csucs+1;
%                     Elements_new(i_elemek,12)=uj_csucs+2;
%                     uj_csucs_koord0=(3*Nodes(c,:)+Nodes(a,:))/4;
%                     uj_csucs_koord1=(2*Nodes(c,:)+2*Nodes(a,:))/4;
%                     uj_csucs_koord2=(Nodes(c,:)+3*Nodes(a,:))/4;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([c a], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[c uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs+1 uj_csucs+2];
%                         Neumann_elek_bovitett(uj_neumann+3,:)=[uj_csucs+2 a];
%                         uj_neumann=uj_neumann+4;
%                     end
%                 end
%                 
%                 % forditva
%                 if isequal([b a] , el)                   
%                     Elements_new(i_elemek,6)=uj_csucs;
%                     Elements_new(i_elemek,5)=uj_csucs+1;
%                     Elements_new(i_elemek,4)=uj_csucs+2;
%                     uj_csucs_koord0=(3*Nodes(b,:)+Nodes(a,:))/4;
%                     uj_csucs_koord1=(2*Nodes(b,:)+2*Nodes(a,:))/4;
%                     uj_csucs_koord2=(Nodes(b,:)+3*Nodes(a,:))/4;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([b a], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[b uj_csucs+2];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs+2 uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs+1 uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+3,:)=[uj_csucs a];
%                         uj_neumann=uj_neumann+4;
%                     end
%                 elseif isequal([c b] , el)
%                     Elements_new(i_elemek,9)=uj_csucs;
%                     Elements_new(i_elemek,8)=uj_csucs+1;
%                     Elements_new(i_elemek,7)=uj_csucs+2;
%                     uj_csucs_koord0=(3*Nodes(c,:)+Nodes(b,:))/4;
%                     uj_csucs_koord1=(2*Nodes(c,:)+2*Nodes(b,:))/4;
%                     uj_csucs_koord2=(Nodes(c,:)+3*Nodes(b,:))/4;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([c b], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[c uj_csucs+2];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs+2 uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs+1 uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+3,:)=[uj_csucs b];
%                         uj_neumann=uj_neumann+4;
%                     end
%                 elseif isequal([a c] , el)
%                     Elements_new(i_elemek,12)=uj_csucs;
%                     Elements_new(i_elemek,11)=uj_csucs+1;
%                     Elements_new(i_elemek,10)=uj_csucs+2;
%                     uj_csucs_koord0=(3*Nodes(a,:)+Nodes(c,:))/4;
%                     uj_csucs_koord1=(2*Nodes(a,:)+2*Nodes(c,:))/4;
%                     uj_csucs_koord2=(Nodes(a,:)+3*Nodes(c,:))/4;
%                     % neumann perem
%                     if ~isempty(Neumann_pontok) && ismember([a c], Neumann_elek,'rows')
%                         Neumann_elek_bovitett(uj_neumann,:)=[a uj_csucs+2];
%                         Neumann_elek_bovitett(uj_neumann+1,:)=[uj_csucs+2 uj_csucs+1];
%                         Neumann_elek_bovitett(uj_neumann+2,:)=[uj_csucs+1 uj_csucs];
%                         Neumann_elek_bovitett(uj_neumann+3,:)=[uj_csucs c];
%                         uj_neumann=uj_neumann+4;
%                     end
%                 end
%             end
%             Nodes_new(uj_csucs,:)=uj_csucs_koord0;
%             Nodes_new(uj_csucs+1,:)=uj_csucs_koord1;
%             Nodes_new(uj_csucs+2,:)=uj_csucs_koord2;
%             uj_csucs=uj_csucs+3;
%         end
% 
%         % bubble points
%         for i_elemek=1:nElemek
%             haromszog=Elements(i_elemek,:);
%             a=haromszog(1);
%             b=haromszog(2);
%             c=haromszog(3);
%             
%             Elements_new(i_elemek,13:15)=[uj_csucs uj_csucs+1 uj_csucs+2];
%             Nodes_new(uj_csucs,:)=(2*Nodes(a,:)+Nodes(b,:)+Nodes(c,:))/4;
%             Nodes_new(uj_csucs+1,:)=(Nodes(a,:)+2*Nodes(b,:)+Nodes(c,:))/4;
%             Nodes_new(uj_csucs+2,:)=(Nodes(a,:)+Nodes(b,:)+2*Nodes(c,:))/4;
%             uj_csucs=uj_csucs+3;
%         end
%         
%         % "s�r�" elemek
%         Elements_new2=zeros(elemek_szama*poly_deg^2,3);
%         for i_elemek=1:elemek_szama
%             elem=Elements_new(i_elemek,:);
% 
%             uj_elemek(1,:)=Elements_new(i_elemek,[1 4 12]);
%             uj_elemek(2,:)= Elements_new(i_elemek,[4 5 13]);
%             uj_elemek(3,:)= Elements_new(i_elemek,[5 6 14]);
%             uj_elemek(4,:)= Elements_new(i_elemek,[6 2 7]);
%             uj_elemek(5,:)= Elements_new(i_elemek,[12 4 13]);
%             uj_elemek(6,:)= Elements_new(i_elemek,[13 5 14]);
%             uj_elemek(7,:)= Elements_new(i_elemek,[14 6 7]);
%             uj_elemek(8,:)= Elements_new(i_elemek,[12 13 11]);
%             uj_elemek(9,:)= Elements_new(i_elemek,[13 14 15]);
%             uj_elemek(10,:)=Elements_new(i_elemek,[14 7 8 ]);
%             uj_elemek(11,:)=Elements_new(i_elemek,[11 13 15]);
%             uj_elemek(12,:)=Elements_new(i_elemek,[15 14 8]);
%             uj_elemek(13,:)=Elements_new(i_elemek,[11 15 10]);
%             uj_elemek(14,:)=Elements_new(i_elemek,[15 8 9]);
%             uj_elemek(15,:)=Elements_new(i_elemek,[10 15 9]);
%             uj_elemek(16,:)=Elements_new(i_elemek,[10 9 3]);
% 
% 
%             Elements_new2(1+(i_elemek-1)*16:16+(i_elemek-1)*16,:)=uj_elemek;
%         end
end
%        --- SCRIPT ---
%% time
time=toc;
disp(['Mesh preprocessing: ',num2str(time),' s']);