function [nu,kappa,e_1] = curve_normal_and_curvature(phi_eval,gamma,varargin)
% Computing normal vector and curvature of a parametrised curve gamma.
% 
% Inputs:   - 'gamma' an anonymous function: gamma:[a,b] \to R^3
%           - 'Nodes' for evaluation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% derivatives
if length(varargin)<2
    d_gamma = matlabFunction(diff(sym(gamma)));
    d2_gamma = matlabFunction(diff(sym(d_gamma)));
else
    d_gamma = varargin{1};
    d2_gamma = varargin{2};
end

%% evaluating the inputs
% ga = gamma(phi_eval);
d_ga = d_gamma(phi_eval);
d2_ga = d2_gamma(phi_eval);


%% tangential vector 
% e_1 = \gamma'(r) / \|\gamma'(r)\|_2
% e_1 = @(r) d_gamma(r) ./ vecnorm(d_gamma(r),2,2);
nrm_d_ga = vecnorm(d_ga,2,2);
e_1 = d_ga ./ nrm_d_ga;

%% normal vector 
% nu(r) = e_2(r) = \bar e_2(r) / \|\bar e_2(r)\|_2 with \bar e_2(r) = \gamma''(r) - (\gamma''(r) \cdot e_1) e_1
bar_e_2 = d2_ga  - sum(d2_ga.*e_1 , 2) .* e_1;
nu = bar_e_2 ./ vecnorm(bar_e_2,2,2);

%% binormal vector
B = cross( e_1 , nu );

%% curvature
% \kappa(r) = \|\gamma'(r) \times \gamma''(r)\|_2 / \|\gamma'(r)\|_2^3
% Note that this formula is signless!!!!

% cross_of_d_ga = cross(d_ga,d2_ga);
% nrm_cross_of_d_ga = vecnorm(cross_of_d_ga,2,2);
% kappa_nosgn =  vecnorm( cross(d_ga,d2_ga) ,2,2) ./ (nrm_d_ga.^3);

% For plane curves (parametrised in R^3) use:
% \kappa(r) = det(\gamma'(r) , \gamma''(r) , e_3) / \|\gamma'(r)\|_2^3
to_det(1:3,1,:) = d_ga.';
to_det(1:3,2,:) = d2_ga.';
to_det(1:3,3,:) = B.';
for ind = 1:length(to_det)
    det_comp(ind,1) = det(to_det(:,:,ind));
end
kappa = det_comp ./ (nrm_d_ga.^3);

%% using outward normal
% !!! Added a negative sign for outward normal. !!!
nu = - sign(kappa) .* nu;


% disp('')
end