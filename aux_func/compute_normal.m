function normal_vec = compute_normal(Nodes,Elements_plot)
% 
% Compute outward unit normal vector based on mesh nodes and elements.
% 
% IMPORTANT: requires CLOCKWISE orientation of element list.
% Otherwise computes irrelevant data.
% 
% Note that DistMesh already provides such an orientation.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

normal_vec = zeros(length(Nodes),3);

E = Elements_plot;
E(:,4) = 0; % marked as not-handeled
E(:,5) = 0; % for storing element area

list_of_Elements = ( 1 : length(E) ).';

normals_on_elements = zeros(length(E),3);

for ind = 1 : length(Nodes)
    % determine patch around the node
    patch_ind_at_node = ((E(:,1)-ind).^2 < 0.5 ) | ((E(:,2)-ind).^2 < 0.5 ) | ((E(:,3)-ind).^2 < 0.5 );
    patch_list_at_node = list_of_Elements(patch_ind_at_node);
    
    % compute the normals along the patch, and average it
    normal_at_node = [0 0 0];
    patch_area = 0;
    for pind = patch_list_at_node.'
%         hold on;
        if E(pind,4) == 0
            % compute the normal (positively (clowcwise) oriented elements)
            normal_pre_norm = cross(Nodes(E(pind,3),:)-Nodes(E(pind,1),:),Nodes(E(pind,2),:)-Nodes(E(pind,1),:));
            % area current element
            element_area = norm(normal_pre_norm,2);
            % the normal vector on the element 
            normal_on_element = normal_pre_norm / element_area;
            
            
%             trisurf(E(pind,1:3),Nodes(:,1),Nodes(:,2),Nodes(:,3), 0 );
%             C_E_pind = sum( Nodes(E(pind,1:3),:) , 1 ) / 3;
%             quiver3(C_E_pind(:,1),C_E_pind(:,2),C_E_pind(:,3),normal_on_element(:,1),normal_on_element(:,2),normal_on_element(:,3))
                        
            % add it to normal
            normal_at_node = normal_at_node + element_area * normal_on_element;
            % save and mark as computed
            normals_on_elements(pind,:) = normal_on_element;
            E(pind,4) = 1;              % marked as computed
            E(pind,5) = element_area;   % storing area
        elseif E(pind,4) == 1
            % the normal is already computed, add it to average
            normal_at_node = normal_at_node + E(pind,5) * normals_on_elements(pind,:);
        end
    end
%     hold off;
%     drawnow
    normal_vec(ind,:) = normal_at_node / norm(normal_at_node);
%     disp('')
end

end