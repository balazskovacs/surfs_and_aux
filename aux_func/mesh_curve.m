function [Nodes,Elements,Elements_plot,phi_eval] = mesh_curve(gamma,a,b,dof,poly_deg,varargin)
% Generating an interpolated mesh for the parametrised curve gamma.
% 
% Inputs:   - 'gamma' an anonymous function: gamma:[a,b] \to R^3
%           - 'a' and 'b' definig the interval [a,b] for the parametrisation
%           - 'dof' degrees of freedom in case of a LINEAR interpolation
%           - 'poly_deg' polynomial degree of interpolation: 1 or 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isempty(varargin)
    surf_name = 'general_parametrisation';
else
    surf_name = varargin{1};
end

%--------------------------------------------------------------------------
%% standard parametrisations
if ~strcmp(surf_name , 'Angenent_oval')
    %% parametrisation data
    phi = ( a : (b-a)/dof : b-(b-a)/dof ).';

    %% nodes and elements for linear surface elements
    Nodes = gamma(phi);

    Elements(:,1) = (1:dof);
    Elements(:,2) = [2:dof 1];

    %% add nodes and elements if deg = 2
    % also return Elements_plot
    if poly_deg == 1
        Elements_plot = [ unique( reshape( Elements.' , [1 numel(Elements)] ) .' , 'stable' ) ; Elements(1,1)];
        phi_eval = phi;
    elseif poly_deg == 2
        % parametrisation data
        phi_mid = ( a + 0.5*(b-a)/dof : (b-a)/dof : b-0.5*(b-a)/dof ).';

        Nodes_mid = gamma(phi_mid);
        Nodes = [ Nodes; Nodes_mid ];

        Elements = [Elements (dof+1:2*dof)'];

        Elements_plot = [ unique( reshape( Elements(:,[1 3 2]).' , [1 numel(Elements)] ) .' , 'stable' ) ; Elements(1,1)];

        phi_eval = [phi;phi_mid];
    end
    
%--------------------------------------------------------------------------
%% Angenent's oval  
else
	%% parametrisation data
    phi = ( a : (b-a)/dof : b-(b-a)/dof ).';
    
    %% adding some grading to 'phi' at pi/2 and 3*pi/2
%     [~,I] = ismember([pi/2;3*pi/2],phi);
%     phi_add1_pre = linspace( phi(I(1)-1,1) , phi(I(1)+1,1) , (dof-2)/8 ).';
%     phi_add2_pre = linspace( phi(I(2)-1,1) , phi(I(2)+1,1) , (dof-2)/8 ).';
%     phi_add1 = abs( phi_add1_pre -   pi/2 ).^0.25 .* ( phi_add1_pre -   pi/2 ) +   pi/2; 
%     phi_add2 = abs( phi_add2_pre - 3*pi/2 ).^0.25 .* ( phi_add2_pre - 3*pi/2 ) + 3*pi/2; 
%     phi = unique( [phi ; phi_add1 ; phi_add2] );
%     % redefine 'dof'
%     dof = length(phi);
    
    %% nodes and elements for linear surface elements
    Nodes = eval_gamma(gamma,phi);

    Elements(:,1) = (1:dof);
    Elements(:,2) = [2:dof 1];

    %% add nodes and elements if deg = 2
    % also return Elements_plot
    if poly_deg == 1
        Elements_plot = [ unique( reshape( Elements.' , [1 numel(Elements)] ) .' , 'stable' ) ; Elements(1,1)];
        phi_eval = phi;
    elseif poly_deg == 2
        % parametrisation data
        phi_mid = [0.5*(phi(1:end-1,1) + phi(2:end,1)); 0.5*(phi(end,1)+b)];
        
        Nodes_mid = eval_gamma(gamma,phi_mid);
        Nodes = [ Nodes; Nodes_mid ];

        Elements = [Elements (dof+1:2*dof)'];

        Elements_plot = [ unique( reshape( Elements(:,[1 3 2]).' , [1 numel(Elements)] ) .' , 'stable' ) ; Elements(1,1)];

        phi_eval = [phi;phi_mid];
    end
    
    % shifting to origin
    max_Nodes2 = max( abs( Nodes(:,2) ) );
    Nodes(:,2) = Nodes(:,2) - max_Nodes2/2;
    
end

end

function G = eval_gamma(gamma,r)

for ind = 1:length(r)
    G(ind,:) = gamma( r(ind) );
end

end
