function [Nodes,Elements,Elements_plot] = load_num_mesh_p2(surf_name,n,fd,grad_fd)
% 
% Loading or precomputing (and projecting) P2 mesh for KLL algorithm.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if exist(['../surfs/',surf_name,'_nodes_p2_',num2str(n),'.txt'], 'file')==2
        Nodes=load(['../surfs/',surf_name,'_nodes_p2_',num2str(n),'.txt']);
        Elements=load(['../surfs/',surf_name,'_elements_p2_',num2str(n),'.txt']);
        Elements_plot=load(['../surfs/',surf_name,'_elements_plot_p2_',num2str(n),'.txt']);
    else
        Nodes=load(['../surfs/',surf_name,'_nodes',num2str(n),'.txt']);
        Elements=load(['../surfs/',surf_name,'_elements',num2str(n),'.txt']);
        [~,~,Nodes,Elements,Elements_plot]=preprocess(Nodes,Elements,2);
        
        % projecting back to the surface using distance function fd
%         Nodes = lift(Nodes); % for Angenent torus
        d = fd(Nodes); % distance function
        dgrad=grad_fd(Nodes); % gradient
        dgrad2=dgrad(:,1).^2+dgrad(:,2).^2+dgrad(:,3).^2;
        Nodes = Nodes-[d.*dgrad(:,1)./dgrad2,d.*dgrad(:,2)./dgrad2,d.*dgrad(:,3)./dgrad2];
        
        % saving
        save(['../surfs/',surf_name,'_nodes_p2_',num2str(n),'.txt'], 'Nodes', '-ASCII');
        save(['../surfs/',surf_name,'_elements_p2_',num2str(n),'.txt'], 'Elements', '-ASCII');
        save(['../surfs/',surf_name,'_elements_plot_p2_',num2str(n),'.txt'], 'Elements_plot', '-ASCII');
end