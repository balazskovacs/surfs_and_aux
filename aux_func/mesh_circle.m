function [Nodes,Elements,Elements_plot] = mesh_circle(dof,poly_deg)

% dof = 9; poly_deg = 2;

%% parametrisation data
a = 0;
b = 2*pi;
phi = ( a : (b-a)/dof : b-(b-a)/dof ).';

%% nodes and elements for linear surface elements
Nodes(:,1) = cos(phi);
Nodes(:,2) = sin(phi);
Nodes(:,3) = 0;

Elements(:,1) = (1:dof);
Elements(:,2) = [2:dof 1];

%% add nodes and elements if deg = 2
% also return Elements_plot
if poly_deg == 1
    Elements_plot = [ unique( reshape( Elements.' , [1 numel(Elements)] ) .' , 'stable' ) ; Elements(1,1)];
elseif poly_deg == 2
    % parametrisation data
    phi_mid = ( a + 0.5*(b-a)/dof : (b-a)/dof : b-0.5*(b-a)/dof ).';

    Nodes = [ Nodes; [cos(phi_mid) sin(phi_mid) zeros(dof,1)] ];

    Elements = [Elements (dof+1:2*dof)'];
    
    Elements_plot = [ unique( reshape( Elements(:,[1 3 2]).' , [1 numel(Elements)] ) .' , 'stable' ) ; Elements(1,1)];
end

% disp('')