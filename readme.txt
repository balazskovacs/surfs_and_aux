This git repository collection of MATLAB codes, files, scripts, etc. which are 
required for all other packages published by the author.

The useres should copy these folders into the same parent-directory as the used package.

Copyright (c) 2022, Bal�zs Kov�cs (balazs.kovacs@ur.de).

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 3 as published by the
Free Software Foundation; either version 3 of the License, or any later version.

This program is distributed "as is", in the hope that it will be useful, but
without warranty of any kind, express or implied.
See the GNU General Public License for more details.

If you use 'MCF diff' in any program, project, or publication, please acknowledge
its authors by adding a reference to the paper: 
C. M. Elliott, H. Garcke, and B. Kov�cs.
Numerical analysis for the interaction of mean curvature flow and 
diffusion on closed surfaces. February 2022.
(We appreciate if you also let the authors know via e-mail.)
